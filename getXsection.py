from __future__ import print_function
import sys, os

def readXsectionFile(infile):
    '''Read the Top Data Preparation file'''

    with open(infile,'r') as f:
        lines = f.readlines()

    # Get rid of the comments
    lines = [l for l in lines if not(l.startswith('#'))]
    # Get rid of the new lines
    lines = [l.split() for l in lines]
    lines = [l for l in lines if len(l)>0]
    lines = [[l[0],l[1:]] for l in lines]
    lines = dict(lines)
    # print(lines['411097'])
    return lines


pico2femto = 1e3
TDP = './XSection-MC16-13TeV.data'

def main(inputDAODFile,outFile):
    metadata = readXsectionFile(TDP)

    # Read the input file
    with open(inputDAODFile,'r') as f:
        lines = f.readlines()
    
    lines = [l for l in lines if not(l.startswith('#'))]
    lines = [l.strip('\n') for l in lines]
    dsids = [l.split('.')[1] for l in lines]

    outData = {}
    failedDSIDs = []

    for dsid in dsids:
        print("Extracting xsection for {0}".format(dsid))
        try:
            fields = metadata[dsid]
        except KeyError:
            print("Cannot find xsection data for {0}, skipping ...".format(dsid))
            failedDSIDs.append(dsid)
            continue

        xsection, kFactor = float(fields[0]), float(fields[1])
        if xsection==0. or kFactor==0.:
            print("Zero xsection or k-factor, skipping ...")
            failedDSIDs.append(dsid)
            continue

        outData[dsid] = xsection*kFactor#*pico2femto

    with open(outFile,'w+') as outF:
        header = '#DSID\txsection (pb)\n'
        outF.write(header)
        for key,value in outData.items():
            outF.write("{0}\t{1}\n".format(key,value))

    print("Falied to get xsection for {0}".format(failedDSIDs))
    return

if __name__ == '__main__':
    inputDAODFile = './infile.txt'
    outFile = './xsections.txt'

    main(inputDAODFile,outFile)
