#!/usr/bin/env python
"""
Python script for rucio
author: D. Panchal
name: pyRucio.py
date: June 2, 2019
"""
from __future__ import print_function
import os,shutil,sys,errno
import glob
import time
import argparse
from itertools import groupby
import logger

try:
    import rucio.client
except:
    print(logger.FAIL + "DANGER! DANGER! DANGER!" + logger.ENDC)
    print("Could not find rucio client. If you use setupATLAS (you should) then do")
    print("lsetup rucio")
    sys.exit(1)

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(path): pass
        else: raise

def uniqueAmiTag(origTag):
    tags = origTag.split('_')
    etags,rtags,ptags,stags = [],[],[],[]
    for tag in tags:
        if 'r' in tag:
            rtags.append(tag)
        if 'e' in tag:
            etags.append(tag)
        if 's' in tag or 'a' in tag:
            stags.append(tag)
        if 'p' in tag:
            ptags.append(tag)

    return "_".join([etags[0],stags[0],rtags[0],ptags[0]])

class RucioClient:
    # identifiers for different data types
    mc16a  = '_r9364'
    mc16d  = '_r10201'
    mc16e  = '_r10724'
    data15 = 'data15'
    data16 = 'data16'
    data17 = 'data17'
    data18 = 'data18'

    def __init__(self,rucioClient,debug=False):
        self._rucio = rucioClient
        idFields = rucioClient.whoami()
        self._status = str(idFields['status'])
        self._email = str(idFields['email'])
        self._username = str(idFields['account'])
        self._scope = 'user.'+self._username
        self.debug = debug

    # Flexing
    def __repr__(self):
        return 'Client: {0}\nEmail: {1}\nStatus: {2}'.format(self._username,self._email,self._status)

    def listScopes(self):
        """List scopes of the user"""
        return self._rucio.list_scopes_for_account('self._username')

    def listDatasets(self,datasetPattern,dtype='all'):
        """Returns the dataset(s) matching the dataset pattern"""
        datasets = []
        filesFound = self._rucio.list_dids(scope=self._scope, filters={'name':datasetPattern},type=dtype)
        for file in filesFound:
            datasets.append(str(file))

        return datasets

    def listFiles(self,dataset):
        try:
            filesFound = self._rucio.list_files(scope=self._scope, name=dataset)
        except:
            print(logger.FAIL + "No files found in {0}".format(dataset) + logger.ENDC)
            return []

        didFiles = []
        for file in filesFound:
            didFiles.append(str(file['name']))

        return didFiles

    def searchDatasets(self,dsid,campaign,date,type='NTUP_output.root'):
        datasetPattern = ".".join([self._scope,dsid])+'.'+'*'+'.'+date+'.'+campaign+'.'+type
        if self.debug: print(datasetPattern)
        
        return self.listDatasets(datasetPattern)

    def detachDatasets(self,datasetPattern,didSuffix):
        """Create new datasets from parent dataset"""
        datasetsFound = self.listDatasets(datasetPattern)
        if len(datasetsFound)==0:
            print(logger.ENDC + "No dataset(s) found matching the pattern {}".format(datasetPattern) + logger.ENDC)
            return

        previousDSID = ''
        newDIDName = ''

        for i,dataset in enumerate(datasetsFound):
            # The date created is used for the new DID
            dateCreated = self._rucio.get_metadata(self._scope,dataset)['created_at'].strftime('%Y-%m-%d')
            datasetFiles = sorted(self.listFiles(dataset), key=lambda x: x.split('.')[2])
            print(logger.OKBLUE + "Found {0} files in parent dataset {1}".format(len(datasetFiles),dataset) + logger.ENDC)
            # Group the files in the dataset by DSIDs
            for dsid,fileItr in groupby(datasetFiles, key=lambda x: x.split('.')[2]):
                # Convert the iterable into a list
                dsetFiles = list(fileItr)
                file = dsetFiles[0]
                fileNameSplitDot = file.split('.')
                currentDSID = fileNameSplitDot[2]
                amiTag = uniqueAmiTag(fileNameSplitDot[5])
                # Create a new DID 
                if currentDSID != previousDSID:
                    previousDSID = currentDSID
                    newDIDName = ".".join(fileNameSplitDot[:5]+[amiTag,dateCreated])
                    if self.mc16a in file:
                        newDIDName += '.mc16a.{0}'.format(didSuffix)
                    elif self.mc16d in file:
                        newDIDName += '.mc16d.{0}'.format(didSuffix)
                    elif self.mc16e in file:
                        newDIDName += '.mc16e.{0}'.format(didSuffix)
                    elif self.data15 in file:
                        newDIDName += '.data15.{0}'.format(didSuffix)
                    elif self.data16 in file:
                        newDIDName += '.data16.{0}'.format(didSuffix)
                    elif self.data17 in file:
                        newDIDName += '.data17.{0}'.format(didSuffix)
                    elif self.data18 in file:
                        newDIDName += '.data18.{0}'.format(didSuffix)

                    print(logger.OKGREEN + "Creating new dataset: {0}".format(newDIDName) + logger.ENDC)
                    try:
                        self._rucio.add_did(scope=self._scope,name=newDIDName,type='dataset',lifetime=157784630) # 5 years
                    except: 
                        print(logger.WARNING + "{} already exists, appending files to existing dataset".format(newDIDName) + logger.ENDC)
                        continue
                print("Adding files: ")
                for f in dsetFiles:
                    print(f)
                # Rucio requires multiple files to be separated by a space
                command = 'rucio attach {0} {1}'.format(newDIDName,' '.join(dsetFiles))
                # print(command)
                os.system(command)

    def checkValidDataset(self,dataset):
        return len(self.listDatasets(dataset))>=1

    def download(self,dataset,downloadDir='./',nrandom=2,no_subdir=True,nthreads=2):
        """Generic download function"""

        isValidDataset = self.checkValidDataset(dataset)
        if not isValidDataset:
            print(logger.WARNING + "WARNING: Invalid dataset, cannot download files" + logger.ENDC)
            return

        if downloadDir[-1] != '/':
            downloadDir += '/'

        mkdir_p(downloadDir)
        datasetFiles = self.listFiles(dataset)
        suffix = ''
        if len(datasetFiles)>1:
            suffix += 's'
    
        print(logger.OKBLUE + "Found {0} file{1} in dataset".format(len(datasetFiles),suffix) + logger.ENDC)

        command = 'rucio download {0}:{1} --dir={2} \\\n'.format(self._scope,dataset,downloadDir)
        if nrandom>0:
            command += ' --nrandom {0}'.format(nrandom)
        if no_subdir:
            command += ' --no-subdir'

        command += ' --ndownloader {0}'.format(nthreads)
        
        if self.debug:
            print(command)
        else:    
            for l in os.popen(command).readlines():
                print(">> ",l.strip())

    def downloadAndMove(self,dataset,path='./',**kwargs):
        """Download files from dataset and create new directories"""
        datasetsFound = self.listDatasets(dataset)
        if len(datasetsFound)>20:
            print(logger.WARNING + "WARNING! Too many datasets to download." + logger.ENDC)

        tempDir = path+'/temp'
        mkdir_p(tempDir)
        kwargs['downloadDir'] = tempDir
        for dset in datasetsFound:
            print(dset)
            self.download(dset,**kwargs)
            self.moveFiles(tempDir)

    def moveFiles(self,downloadDirectory):
        if self.debug:
            return
        downloadedFiles = os.listdir(downloadDirectory)
        moveDirectory = ' '
        previousDSID = ' '
        print(logger.OKBLUE + "Moving downloaded files" + logger.ENDC) 
        filePath = downloadDirectory.strip('temp')
        for file in downloadedFiles:
            fileNameSplitDot = file.split('.')
            fileNameSplitSplash = file.split('/')
            currentDSID = fileNameSplitDot[2]
            if currentDSID != previousDSID:
                previousDSID = currentDSID
                moveDirectory = filePath+'/'+currentDSID+'_selections'
                print(logger.OKBLUE + "Creating directory: {0}".format(moveDirectory) + logger.ENDC)
                mkdir_p(moveDirectory)

            newFileNameList = fileNameSplitDot[:2]+fileNameSplitDot[-4:]
            newFileName = ".".join(newFileNameList)
            destination = "/".join([moveDirectory,newFileName])
            source = "/".join([downloadDirectory,file])
            shutil.move(source,destination)

        os.rmdir(downloadDirectory)

if __name__ == '__main__':
    __author__ = "D. Panchal"
    __doc__ = """Python script for rucio"""
    try:
        rucio = rucio.client.Client()
    except:
        print(logger.FAIL + "DANGER! DANGER! DANGER!" + logger.ENDC)
        print("Could not find rucio.client.Client")
        print("Did you do voms-proxy-init -voms atlas ?")
        sys.exit(1)

    parser = argparse.ArgumentParser(usage='%(prog) s [options]', description=__doc__)
    parser.add_argument(dest='pattern', type=str,
                        help='Dataset pattern or DSID')
    subparser = parser.add_subparsers(dest='command') 

    parser_detach = subparser.add_parser('detach',
                                         help="Create new datasets and attach relevant files from parent dataset")
    parser_detach.add_argument('--didSuffix', type=str, default="NTUP_output.root",
                                help="Create new datasets with the following suffix")

    parser_download = subparser.add_parser('download',
                                           help='Download dataset files')
    parser_download.add_argument('--path', type=str, default='./',
                                 help='Destination to store the downloaded files')   
    parser_download.add_argument('--nrandom', type=int, default=-1, 
                                 help='Number of random files. Default: all files')
    parser_download.add_argument('--no-subdir', action='store_true', default=True, dest='no_subdir',
                                 help="Create a subdirectory")
    parser_download.add_argument('--move', action='store_true',
                                 help='Move downloaded files')

    parser_search = subparser.add_parser('search',
                                help='List dids matching search fields')
    parser_search.add_argument('--campaign', type=str,
                                help="Data type campaign, e.g. mc16a")
    parser_search.add_argument('--date', type=str,
                                help="Dataset creation date")
    parser_search.add_argument('--didSuffix', type=str, default="NTUP_output.root",
                                help="Search for log or ntuple files")
    parser_search.add_argument('--download',action='store_true',default=False,
                                help='Download searh DSIDs')

    parser_get = subparser.add_parser('get',
                                help='List and download dids matching search fields')
    parser_get.add_argument('--campaign', type=str,
                                help="Data type campaign, e.g. mc16a")
    parser_get.add_argument('--date', type=str,
                                help="Dataset creation date")
    parser_get.add_argument('--path', type=str, default='./',
                                 help='Destination to store the downloaded files')   
    parser_get.add_argument('--nrandom', type=int, default=-1, 
                                 help='Number of random files. Default: all files')
    parser_get.add_argument('--no-subdir', action='store_true', default=True, dest='no_subdir',
                                 help="Create a subdirectory")
    parser_get.add_argument('--move', action='store_true',
                                 help='Move downloaded files')
    parser_get.add_argument('--didSuffix', type=str, default="NTUP_output.root",
                                help="Download for log or ntuple files")
    parser_get.add_argument('--nthreads', type=int, default=2,
                                help="Number of threads to download files. (Default 2)")

    parser_list = subparser.add_parser('list',
                                        help="List the files for the dataset")
    parser_list.add_argument('--campaign', type=str,
                            help="Data type campaign, e.g. mc16a")
    parser_list.add_argument('--date', type=str,
                            help='Dataset creation date (separated by underscores)')
    parser_list.add_argument('--didSuffix', type=str, default="NTUP_output.root",
                            help="List log or ntuple files")

    args = parser.parse_args()
    datasetPattern = args.pattern.split(',')
    # print(datasetPattern)
    # dataset = 'user.dpanchal.411092.PhPy8EG.DAOD_BPHY9.e6852_a875_r10724_p3639.2019-05-31.mc16e.NTUP_output.root'
    client = RucioClient(rucio,debug=False)
    # print(client)
    for dset in datasetPattern:
        if args.command == 'detach':
            client.detachDatasets(dset,args.didSuffix)
        if args.command == 'download':
            path = args.path
            nrandom = args.nrandom
            no_subdir = args.no_subdir
            if args.move:
                client.downloadAndMove(dset,path=path,nrandom=nrandom,no_subdir=no_subdir)
            else:
                client.download(dset,downloadDir=path,nrandom=nrandom,no_subdir=no_subdir)
        if args.command == 'search':
            campaign = args.campaign
            date = args.date
            dtype = args.didSuffix
            didsFound = client.searchDatasets(dset,campaign=campaign,date=date,type=dtype)
            print(logger.OKBLUE + "For DSID {0}, found:".format(dset) + logger.ENDC)
            for did in didsFound:
                print(did)

        if args.command == 'get':
            campaign = args.campaign
            date = args.date
            path = args.path
            nrandom = args.nrandom
            no_subdir = args.no_subdir
            dtype = args.didSuffix
            nthreads = args.nthreads
            didsFound = client.searchDatasets(dset,campaign=campaign,date=date,type=dtype)
            if len(didsFound)==0:
                print(logger.FAIL + 'No matching datasets found...' + logger.ENDC)
                continue
            elif len(didsFound)>1:
                print(logger.OKBLUE + "Found {0} datasets, which one to download?".format(len(didsFound)) + logger.ENDC)
                idx = int(raw_input("Enter index: "))
                dataset = didsFound[idx]
            else:
                dataset = didsFound[0]

            if args.move:
                client.downloadAndMove(dataset,path=path,nrandom=nrandom,no_subdir=no_subdir,nthreads=nthreads)
            else:
                client.download(dataset,downloadDir=path,nrandom=nrandom,no_subdir=no_subdir,nthreads=nthreads)

        if args.command == 'list':
            campaign = args.campaign
            date = args.date
            dtype = args.didSuffix
            didsFound = client.searchDatasets(dset,campaign=campaign,date=date,type=dtype)
            print(logger.OKBLUE + "For DSID {0}:".format(dset) + logger.ENDC)
            for did in didsFound:
                filesFound = client.listFiles(did)
                if len(filesFound)==0:
                    print(logger.FAIL + "No files found..." + logger.ENDC)
                else:
                    print(logger.OKGREEN + "Found {0} file(s)".format(len(filesFound)) + logger.ENDC)
                    print(*filesFound, sep='\n')